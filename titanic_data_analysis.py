import numpy
import pandas
import matplotlib.pyplot as pyplot

# Step 0: get raw data
passenger_data = pandas.read_csv('titanic_training_data.csv', ',')

# 1: Clean data
column_names = ['Passenger_ID', 'Survived', 'Pclass', 'Name', 'Sex', 'Age', 'SibSp',
       'Parch', 'Ticket', 'Fare', 'Cabin', 'Embarked']
passenger_data.columns = column_names

# pyplot.scatter(passenger_data['Survived'], passenger_data['Fare'])
# pyplot.plot(kind='bar', x=passenger_data.Survived, y=passenger_data.Fare)

# print(passenger_data.iloc[:5])
# print(passenger_data.dtypes)
# print(passenger_data.head(6))
# print(passenger_data['Name'])
